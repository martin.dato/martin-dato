#!/bin/bash


args=( $@ )

function fun_null () {

	if [ -z "$args" ]; then

 	  echo "Por favor escribir un argumento, puede utilizar el comando -h / --help"
	  return 1
	  exit 1
	
	else
	  return 0
	fi
}
 
function fun_help() {
	  echo "Bienvenido a -help"
	  echo ""
	  echo "Acontinuacion vera informacion util sobre como ejecutar el scrip"
	  echo""
	  echo "Para ejecutar el scritp debera poner un hostname valido, ejemplo: hostname.com"
	  echo ""
	  echo "Puede agregar alguna de las siguientes opciones a su ejecucion."
	  echo ""
	  echo "-C: Define la cantidad de pingueos al host teniendo que definir en n=[un numero entero positvo] , ejemplo: [-C n hostanem.com]"
	  echo ""
	  echo "-T: Devuelve una indicacion de fecha y hora, ejemplo: [-T hostname.com]"
	  echo ""
	  echo "-p: El protocolo usado para el ping ya sea 4=IPv4 o 6=IPv6  ejemplo: [-p [4/6] hostname.com]"
	  echo ""
	  echo "-b: Permite realizar el ping a una IP de broadcast  "
	  echo ""
	  echo "-R: crea un archivo llamado ´resultado-ping.txt´ donde se guardara el resultado mostrado en pantalla."
	  echo "esta opcion debe ejecutarse junto al comando -C ejem: ping -C x -R <hostname/ip>"
	  echo ""
	  sleep 0.2
	  echo "El script detuvo su ejecucion, ningun cambio fue realizado en su sistema. "
	  echo ""
	  exit 0
}

function fun_txt () {

ping $b $timestamp $p $counter ${args[-1]} > resultado-ping.txt

return 0 

}

fun_null

if [ $? -eq 0 ] ; then

	for opcion in "${!args[@]}"; do

		case ${args[$opcion]} in

		-C) cantidad=${args[$(($opcion+1))]}
	  	    counter="-c $cantidad"
		;;
		-T) tiemstamp="-D"
	        ;;
		-p) proto=${args[$(($opcion+1))]}
	            p="$proto"
		;;
		-b) b="-b"
		;;

		-h|--help) fun_help
		;;

		-R) fun_txt
		;;

		esac

	done

ping $b $timestamp $p $counter ${args[-1]}

else

exit 1

fi

