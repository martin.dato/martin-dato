#!/bin/bash


args=( $@ )
 
for opcion in "${!args[@]}"; do

	if [ "$args" = "-help" ] ; then
	  echo ""
	  echo "Bienvenido a -help"
	  echo ""
	  echo "Acontinuacion vera informacion util sobre como ejecutar el scrip"
	  echo""
	  echo "Para ejecutar el scritp debera poner un hostname valido, ejemplo: hostname.com"
	  echo ""
	  echo "Puede agregar alguna de las siguientes opciones a su ejecucion."
	  echo ""
	  echo "-C: Define la cantidad de pingueos al host teniendo que definir en n=[un numero entero positvo] , ejemplo: [-C n hostanem.com]"
	  echo ""
	  echo "-T: Devuelve una indicacion de fecha y hora, ejemplo: [-T hostname.com]"
	  echo ""
	  echo "-p: El protocolo usado para el ping ya sea 4=IPv4 o 6=IPv6  ejemplo: [-p [4/6] hostname.com]"
	  echo ""
	  echo "-b: Permite realizar el ping a una IP de broadcast  "
	  echo ""
	  echo "El script detuvo su ejecucion, ningun cambio fue realizado en su sistema. "
	  echo ""
	  exit 1
	  else

	if [ -z $args ] ;then
		case ${args[$opcion]} in

		-C) cantidad=${args[$(($opcion+1))]}
	  	  	 if [[ $cantidad =~ ^[0-9]+$ ]] ;then
				 counter="-c $cantidad"
			else
				echo "Se coloco un valor no valido para la opcion -C"
			exit 1
			fi
		;;
		-T) tiemstamp="-D"
	        ;;
		-p) proto=${args[$(($opcion+1))]}
			if [[ $proto !=4 && $proto !=6 ]] ;then
				echo "El valor ingresado no corresponde a 4 o 6"
				exit 1
			fi
		    p="$proto"
		;;
		-b) b="-b"
		;;

		esac
	fi
done


ping $b $timestamp $p $counter ${args[-1]}



