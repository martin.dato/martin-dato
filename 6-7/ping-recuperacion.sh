#!/bin/bash


args=( $@ )

#se verifica que se haya colocado algun argumento"
if [ ! -z $args ] ;then
#valido la IP que sea correcta, 1 a 3 digitos y menor a 255
	if [[ ${args[-1]} =~ ^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$ ]] ;then
		oIFS=$IFS
		IFS="."
		ip=(${args[-1]})
		IFS=$oIFS
		if [[ ${ip[0]} -le 255 ]] && [[ ${ip[1]} -le 255 ]] && [[ ${ip[2]} -le 255 ]] && [[ ${ip[3]} -le 255 ]] ;then
			echo ""
		else
			echo "Ingreso un Host/IP invalidos"
			exit 1
		fi
#se valida que sea un host
	else
		if [[ ${args[-1]} =~ ^(([a-zA-Z]|[a-zA-Z][a-zA-Z0-9\-]*[a-zA-Z0-9])\.)*([A-Za-z]|[A-Za-z][A-Za-z0-9\-]*[A-Za-z0-9])$ ]] ;then
			echo ""
		else
			echo "Ingreso un Host/IP invalidos"
			exit 1
		fi

	fi

for opcion in "${!args[@]}"; do

		case ${args[$opcion]} in
#Se comprueba que el numero colocado sea un enetero positivo
		-C) cantidad=${args[$(($opcion+1))]}
			 if [[ $cantidad -ge 1 ]] ;then
				 counter="-c $cantidad"
			else
				echo "Se coloco un parametro no valido para la opcion -C"
			exit 1
			fi
		;;
		-T) tiemstamp="-D"
	        ;;
#Valido el argumento para P sea 4 o 6
		-p) proto=${args[$(($opcion+1))]}
			if [[ $proto != 4 && $proto != 6 ]] ;then
				echo "Se coloco un parametro incorrecto"
				exit 1
			fi
			p="-$proto"
		;;
		-b) b="-b"
		;;
		-h|-H|--help)
		  echo ""
                  echo "Bienvenido a -help"
                  echo ""
                  echo "Acontinuacion vera informacion util sobre como ejecutar el scrip"
                  echo""
                  echo "Para ejecutar el scritp debera poner un hostname/ip valido"
                  echo ""
                  echo "Puede agregar alguna de las siguientes opciones a su ejecucion."
                  echo ""
                  echo "-C: Define la cantidad de pingueos al host teniendo que definir en n=[un numero entero positvo] , ejemplo: [-C n hostanem.com]"
                  echo ""
                  echo "-T: Devuelve una indicacion de fecha y hora, ejemplo: [-T hostname.com]"
                  echo ""
                  echo "-p: El protocolo usado para el ping ya sea 4=IPv4 o 6=IPv6  ejemplo: [-p [4/6] hostname.com]"
                  echo ""
                  echo "-b: Permite realizar el ping a una IP de broadcast  "
                  echo ""
                  echo "El script detuvo su ejecucion, ningun cambio fue realizado en su sistema. "
                  echo ""
                  exit 1

		esac

done

ping $b $timestamp $p $counter ${args[-1]}

else

echo "No se coloco ningun arguento"
exit 1

fi


#ping $b $timestamp $p $counter ${args[-1]}


