#!/bin/bash

args=( $@ )

if [ ! -z $args ] ;then

for opcion in "${!args[@]}"; do

	case ${args[$opcion]} in

		-c|-C) cantidad=${args[$(($opcion+1))]}
		       counter="-c $cantidad"
			if [
		;;
		-t|-T) timestamp="-D"
		;;
		-p|-P) proto=${args[$(($opcion+1))]}
		       p="-$proto"
		;;
		-b|-B) b="-b"
		;;
		-h|-H|--help)
                  echo "Bienvenido a -help"
                  echo ""
                  echo "Acontinuacion vera informacion util sobre como ejecutar el scrip"
                  echo""
                  echo "Para ejecutar el scritp debera poner un hostname/ip validos"
                  echo ""
                  echo "Puede agregar alguna de las siguientes opciones a su ejecucion."
                  echo ""
                  echo "-C: Define la cantidad de pingueos al host teniendo que definir en n=[un numero entero positvo] , ejemplo: [-C n hostanem.com]"
                  echo ""
                  echo "-T: Devuelve una indicacion de fecha y hora, ejemplo: [-T hostname.com]"
                  echo ""
                  echo "-p: El protocolo usado para el ping ya sea 4=IPv4 o 6=IPv6"
                  echo ""
                  echo "-b: Permite realizar el ping a una IP de broadcast  "
                  echo ""
                  exit 1
		;;
	esac
done

ping $b $timestamp $p $counter ${args[-1]}

else

echo "No se coloco ningun argumento"

fi

# se recogeran las opciones del script original para ir realizando las validaciones
if [[ $arg = [Cc] ]] ;then
	if [[ $cantidad = ^[0-9]+$ ]] ;then
		counter="-c $cantidad"	
	else
	echo" se coloco un valor no valido para la opcion -C"
	fi
fi

