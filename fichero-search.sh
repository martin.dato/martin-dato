#!/bin/bash

read -p "Escriba el nombre del fichero: " fichero

if test -e $fichero ;then 
	echo "El fichero existe"
	if test -e $fichero && test -x $fichero && ls -l $fichero>>/dev/null | ls -l $fichero ;then
		read -p "Desea ejecutar el fichero $fichero? Y/N: " ans1
			if [[ $ans1 =~ [Yy] ]];then
			echo "El fichero fue ejecutado"
			else
			echo "El fichero no fue ejecutado"
			exit
			fi
	else
		 read -p "El fichero no tiene permisos de ejecucion, desea otorgarlos? Y/N" ans2
			if [[ $ans2 =~ [Yy] ]];then
			chmod +x $fichero
			echo "Permisos otorgados"
			ls -l $fichero
			else
			echo "Permisos no otorgados"
			fi
	fi
else
	 echo" El fichero no existe"
exit
fi
