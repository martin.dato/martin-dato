#!/bin/bash

#la v es para ver informacion detalla del proceso, la z para comprimir y la h para lenguaje humano

function fun_rsync () {

	read -p  "Desea ver detalles del proceso?: Y/N " ans
if [[ $ans =~ [Yy] ]];then
	rsync -azvh "$origen" "$destino"
else
	read -p "Desea comprimir los datos durante la transferencia?: Y/N " ans2
		if [[ $ans2 =~ [Yy] ]];then
			rsync -az "$origen" "$destino"
		else
			rsync -a "$origen" "$destino"
		fi
fi
}

#-------------------

function fun_ssh () {

	read -p "Colocar user: " user
if [[ -z "$user" ]] ;then 
	echo "Usten no coloco ningun user, el programa se cerrar"
	exit
else
	read -p "Colocar localhost:" host
		if [[ -z "$host" ]] ;then
			echo "Usted no coloco ningun host, el programa se cerrara"
		exit
		else
			ssh-copy-id -i ~/.ssh/id_rsa.pub "$user"@"$host"
		fi	
fi
}

#-------------------

function fun_genssh () {

	read -p "Desea generar una llave?: Y/N " ans3
if [[ $ans3 =~ [Yy] ]] ;then
	ssh-keygen -t rsa

else
	echo "Selecciono N, el programa se cerrara"
exit
fi
}

#--------------------

function fun_null () {

if [ -z "$origen" ] ;then 
	echo "No se escribio ningun caracter o el archivo/directorio son inexistentes, el programa se cerrara"
exit
else
	if [ ! -d "$origen" ] ;then
		if [ ! -f "$origen" ] ;then
			echo "Se escribio un directorio/fichero inexistente, el programa se cerrara"
		exit
		fi
	fi
fi
}

#-------------------

function fun_crontab () {

echo "Los parametros se definen mediante unos asteriscos antes del /origen /destino, que corresponden de izquierda a derecha lo siguiente:"
echo "m: munito, h: hora, dom: dia del mes, mon: mes, dow: dia de la semana => ejem: 00 19 * * * rsync -a /origen /destino"
echo "Una vez escrito el tiempo, rsync y las rutas debera salvar lo echo con 'ctrl + o' seguido para salir de 'ctrl + x'"
	read -p "Desea continuar con crontab?: Y/N " ans
		if [[ $ans =~ [Yy] ]] ;then 
			crontab -e
		else
			echo "Selecciono N, el programa se cerrara"
		exit
		fi
}

#------------------

function fun_help () {

echo ""
echo "Bienvenido al menu de ayuda"
echo ""
echo "Recuerde que puede salir de la ejecucion del programa con la convinacion de teclas 'CTRL + C'"
echo ""
echo "Opcion 2"
echo "El programa le solicitara colocar un directorio o fichero de origen: /ejemplo/ejemplo"
echo "En este punto el programa revisara si se colocaron caracteres, en caso que no. El programa se cerrara."
echo "Seguido se le solicitara el destino para el backup a realizar."
echo "Se le consultara si desea informacion detallada del proceso o no y si desea comprimir los datos durante la tranferencia"
echo ""
echo "Opcion 3"
echo "El programa enviara la llave que usted posea en su equipo al host remoto al que quiere realizar el backup"
echo "Esta opcion cumplira su propisito siempre y cuando tenga una llave generada"
echo "Puede hacerlo en la siguiente opcion"
echo ""
echo "Opcion 4"
echo "Correra un comando para generar su llave, usted podra ponerle password o no."
echo "El comando le dara instrucciones a medida que se ejecute"
echo ""
echo "Opcion 5"
echo "Con esta opcion se podran programar backups automaticos, esta misma informacion se vera cuando se seleccione"
echo "ya que se deberan definir ciertos parametros"
echo "Los parametros se definen mediante unos asteriscos antes del /origen /destino, que corresponden de izquierda a derecha lo siguiente:"
echo "m: munito, h: hora, dom: dia del mes, mon: mes, dow: dia de la semana => ejem: 00 19 * * * rsync -a /origen /destino"
echo "Una vez escrito el tiempo, rsync y las rutas debera salvar lo echo con 'ctrl + o' seguido para salir de 'ctrl + x'"
echo ""
echo "Opcion 6"
echo "Salir del programa"
echo "El programa se cerrara"
echo ""
}

#Menu----------------

echo "Bienvenido al programa de backup"
echo ""
echo "1. Menu de ayuda"
echo "2. Realizar backup"
echo "3. Enviar llave al host remoto"
echo "4. Generar llave"
echo "5. Programar Backup"
echo "6. Salir"

read -p "seleccione una opcion: " opc

case $opc in

	1)	echo ""
		echo "Opcion 1 seleccionada"
		fun_help
	;;
	2) 	echo ""
		echo "Opcion 2 seleccionada"
		sleep 0.2
		echo "Ingresar directorio o archivo de origen"
		read origen
		fun_null
		echo "Ingresar directorio o archivo de destino"
		read destino
		fun_rsync
		echo ""
		echo "Destino: $destino"
		exit 0
	;;
	3)	echo ""
		echo "Opcion 3 seleccionada"
		sleep 0.2
		fun_ssh
	;;
	4)	echo ""
		echo "Opcion 4 seleccionada"
		sleep 0.2
		fun_genssh
	;;
	5) 	echo ""
		echo "Opcion 5 seleccionada"
		sleep 0.2
		fun_crontab
	;;
	6)	echo ""
		echo "Opcion 6 seleccionada"
		echo "El programa se cerrara"
	;;
	*)  	echo ""
		echo "No se selecciono ninguna opcion, el programa se cerrara"
	;;
esac







